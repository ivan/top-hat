/***************************************************************************
 *   Copyright (C) 2015 by Ivan Cukic <ivan.cukic at kde.org>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

import QtQuick 2.1
import QtQuick.Layouts 1.1
import org.kde.telepathy 0.1 as KTp
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.plasmoid 2.0
import org.kde.kquickcontrolsaddons 2.0

import org.kde.plasma.contacts 0.1 as Backend

Item {
    id: root

    Layout.minimumHeight: delegateHeight * 3
    Layout.minimumWidth: 100

    property real delegateHeight: Math.ceil(theme.mSize(theme.defaultFont).height*2)

    property int topContactsHeight: topContactsPanel.y + topContactsPanel.height

    Item {
        id: topPanel

        height: 0 // filterLineEdit.height
        width: parent.width
        visible: false

        PlasmaComponents.TextField {
            id: filterLineEdit

            anchors {
                left:  parent.left
                right: parent.right
                top:   parent.top
            }

            visible: !goOnlineItem.visible && !addAccountItem.visible
            focus: true
            clearButtonShown: true

            placeholderText: i18n("Search Contacts...")

            Keys.onDownPressed   : contactsList.incrementCurrentIndex();
            Keys.onUpPressed     : contactsList.decrementCurrentIndex();
            Keys.onReturnPressed : contactsList.currentItem.clicked();

            onActiveFocusChanged: filterLineEdit.selectAll();
        }
    }

    PlasmaExtras.ScrollArea {

        visible: true // !goOnlineItem.visible && !addAccountItem.visible

        anchors {
            left:   parent.left
            right:  parent.right
            top:    topPanel.bottom
            bottom: parent.bottom
        }

        contentItem: ListView {
            id: contactsList

            clip: true

            model: KTp.ContactsModel {
                id: contactsModel
                accountManager: telepathyManager.accountManager
                presenceTypeFilterFlags: KTp.ContactsModel.HideAllOffline
                globalFilterString: filterLineEdit.text
                sortRoleString: "presenceType"
            }

            boundsBehavior: Flickable.StopAtBounds

            header: Item {
                width: parent.width
                height: topContactsPanel.height
                clip: true

                TopContacts {
                    id: topContactsPanel

                    z: -2

                    width: parent.width

                    // The list should go above the top contacts,
                    // but the contacts can not be forced to always be
                    // on top, fixed, because it can be higher than
                    // the containing window.

                    y: height < contactsList.height ?
                            // If the top contacts panel can fit inside the
                            // window, we want to make it fixed to the top
                            height + contactsList.contentY :

                       contactsList.height > - contactsList.contentY ?
                            // If the user has scrolled the top panel enough
                            // so that its end is shown, offset the panel
                            // just right
                            contactsList.height + contactsList.contentY :

                            // If we are in-between, we need to keep scrolling
                            // until we reach the end
                            0
                }
            }

            delegate: ListContactDelegate {
                height: delegateHeight
            }

            highlightMoveDuration: 1000
        }
    }
}
