/***************************************************************************
 *   Copyright (C) 2015 by Ivan Cukic <ivan.cukic at kde.org>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

import QtQuick 2.1
import QtQuick.Layouts 1.1
import org.kde.telepathy 0.1 as KTp
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.plasmoid 2.0

import org.kde.plasma.contacts 0.1 as Backend
import org.kde.kquickcontrolsaddons 2.0

Item {
    id: root

    onVisibleChanged: {
        if (visible)
            filterLineEdit.forceActiveFocus();
    }

    height: panelFavoriteContacts.height

    function context() {

    }

    function trigger(id, action) {
        console.log("trigger " + id + " / " + action);
        topContactsModel.trigger(id, action);
    }

    property real sizeIncreaseRatio    : 1.3
    property int  minimalTopItemSize   : units.iconSizes.large * 1.8
    property real firstRowDesiredSize  : sizeIncreaseRatio * sizeIncreaseRatio * minimalTopItemSize
    property real secondRowDesiredSize : sizeIncreaseRatio * minimalTopItemSize

    // We do not want the number of items to go below 2
    property int firstRowCount         : Math.max(2, Math.round(width / firstRowDesiredSize))

    // We do not want to have free space in the second row, but at the same time
    // we do not want the number of items to be less than in the first row
    property int secondRowCount        : Math.max(
                                            Math.min(Math.round(width / secondRowDesiredSize),
                                                     contactsRepeater.count - firstRowCount),
                                            firstRowCount)


    property int otherRowCount         : Math.round(width / minimalTopItemSize)

    property int firstRowSize          : width / firstRowCount
    property int secondRowSize         : width / secondRowCount
    property int otherRowSize          : width / otherRowCount

    Flow {
        id: panelFavoriteContacts

        anchors {
            left: parent.left
            right: parent.right
        }

        Repeater {
            id: contactsRepeater

            model: Backend.TopContacts {
                id: topContactsModel
            }
            // model: KTp.ContactsModel {
            //     id: contactsModel
            //     accountManager: telepathyManager.accountManager
            //     presenceTypeFilterFlags: KTp.ContactsModel.HideAllOffline
            //     globalFilterString: filterLineEdit.text
            //     sortRoleString: "presenceType"
            // }

            delegate: TopContactDelegate {
                id: delegate

                width: (index < firstRowCount) ? firstRowSize
                     : (index < firstRowCount + secondRowCount) ? secondRowSize
                     : otherRowSize

                onClicked            : root.trigger(model.url, "")
                onSendEmail          : root.trigger(model.url, "email")
                onStartChat          : root.trigger(model.url, "chat")
                onLinkToActivity     : root.trigger(model.url, "linkToActivity")
                onUnlinkFromActivity : root.trigger(model.url, "unlinkFromActivity")
            }
        }

        move: Transition {
            NumberAnimation { properties: "x,y"; duration: 200 }
        }
    }


}
