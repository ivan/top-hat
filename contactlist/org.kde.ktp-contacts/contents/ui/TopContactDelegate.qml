/***************************************************************************
 *   Copyright (C) 2015 by Ivan Cukic <ivan.cukic at kde.org>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

import QtQuick 2.2

import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.kquickcontrolsaddons 2.0

import "static.js" as S

Item {
    id: root

    enabled: true
    // property bool isCurrent: delegate.ListView.view.currentIndex==index

    signal clicked()
    signal sendEmail()
    signal startChat()
    signal linkToActivity()
    signal unlinkFromActivity()

    // We are square
    height: width
    clip: true

    QPixmapItem {
        id: iconAvatar

        pixmap: model.avatarPixmap
        fillMode: QPixmapItem.PreserveAspectFit

        smooth: true

        anchors {
            fill: parent
            margins: units.smallSpacing / 2
        }
        // QPixmapItem {
        // }
    }

    Rectangle {
        opacity: .8

        height: nameLabel.height

        anchors {
            left        : parent.left
            right       : parent.right
            top         : parent.top

            topMargin   : units.smallSpacing / 2
            leftMargin  : units.smallSpacing / 2
            rightMargin : units.smallSpacing / 2
        }

        gradient: Gradient {
            GradientStop { position: 0.0; color: "black" }
            GradientStop { position: 1.0; color: "transparent" }
        }
    }

    PlasmaCore.IconItem {
        id: favoriteIcon

        // source: "emblem-favorite"
        source: "favorites"

        width: height
        height: parent.width / 6 // units.iconSizes.small

        visible: model.isLinked

        anchors {
            top         : parent.top
            left        : parent.left

            topMargin   : units.smallSpacing
            rightMargin : units.smallSpacing
        }

    }

    PlasmaCore.IconItem {
        id: presenceIcon

        source: model.presenceIcon

        width: height
        height: parent.width / 6 // units.iconSizes.small

        visible: root.state == "normal"

        anchors {
            top         : parent.top
            right       : parent.right

            topMargin   : units.smallSpacing
            rightMargin : units.smallSpacing
        }

    }

    MouseEventListener {
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton

        onClicked: {
            if (mouse.button == Qt.LeftButton) {
                root.clicked();

            } else if (mouse.button == Qt.RightButton) {
                S.toggleContext(root);

            }
        }

        onWheelMoved: {
            contactsList.contentY -= wheel.delta / 8
            if (contactsList.contentY < -topContactsPanel.height) {
                contactsList.contentY = -topContactsPanel.height;
            }
        }
    }

    Rectangle {
        id: nameLabelBackground

        opacity: .8
        color: 'black'
        height: root.showContext ? (parent.height - units.smallSpacing) : nameLabel.height

        Behavior on height { PropertyAnimation { duration: units.longDuration } }

        anchors {
            left         : parent.left
            right        : parent.right
            bottom       : parent.bottom

            bottomMargin : units.smallSpacing / 2
            leftMargin   : units.smallSpacing / 2
            rightMargin  : units.smallSpacing / 2
        }

        Column {

            y: root.showContext ? 0 : nameLabel.height
            Behavior on y { PropertyAnimation { duration: units.longDuration } }

            width: parent.width
            height: parent.height

            PlasmaComponents.ToolButton {
                text: "Chat"
                iconSource: "im-user"

                width: parent.width
                height: parent.height / 3

                onClicked: { root.startChat(); S.toggleContext(root) }
            }

            PlasmaComponents.ToolButton {
                text: "Email"
                iconSource: "mail-send"

                width: parent.width
                height: parent.height / 3

                onClicked: { root.sendEmail(); S.toggleContext(root) }
            }

            PlasmaComponents.ToolButton {
                text: model.isLinked ? "Unpin" : "Pin"
                iconSource: model.isLinked ? "edit-delete" : "favorites"

                width: parent.width
                height: parent.height / 3

                onClicked: {
                    if (model.isLinked) {
                        root.unlinkFromActivity();
                    } else {
                        root.linkToActivity();
                    }
                    S.toggleContext(root);
                }
            }
        }
    }

    PlasmaComponents.Label {
        id: nameLabel

        text: model.display
        elide: Text.ElideRight

        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter

        opacity: root.state == "normal" ? 1 : 0
        Behavior on opacity { PropertyAnimation { duration: units.longDuration } }

        anchors {
            left        : parent.left
            right       : parent.right
            // bottom      : parent.bottom
            top         : nameLabelBackground.top

            leftMargin  : units.smallSpacing
            rightMargin : units.smallSpacing
        }
    }

    property bool showContext: false

    state: showContext ? "context" : "normal"

    states: [
        State {
            name: "normal"
        },
        State {
            name: "context"
        }
    ]
}
