[Desktop Entry]
Name=Contacts
Comment=Displays your presence and contacts
Type=Service
ServiceTypes=Plasma/Applet
Icon=meeting-attending

X-Plasma-API=declarativeappletscript
X-Plasma-MainScript=ui/main.qml
X-Plasma-DefaultSize=200,300
X-Plasma-NotificationArea=true
X-Plasma-NotificationAreaCategory=Communications
X-Plasma-DBusActivationService=org.freedesktop.Telepathy.MissionControl5

X-KDE-PluginInfo-Name=org.kde.ktp-contacts
X-KDE-PluginInfo-Category=Online Services
X-KDE-PluginInfo-Author=David Edmundson
X-KDE-PluginInfo-Email=kde@davidedmundson.co.uk
X-KDE-PluginInfo-Version=@KTP_DESKTOP_APPLETS_VERSION@
X-KDE-PluginInfo-Website=http://telepathy.freedesktop.org/

X-KDE-PluginInfo-Depends=
X-KDE-PluginInfo-License=GPL
X-KDE-PluginInfo-EnabledByDefault=true
X-KDE-ServiceTypes=Plasma/Applet,Plasma/PopupApplet
Type=Service
