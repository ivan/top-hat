/*
 *   Copyright (C) 2015 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "topcontactsmodel.h"

#include <kpeople/widgets/actions.h>
#include <kpeople/widgets/persondetailsdialog.h>
#include <KPeople/PersonData>

#include <kactivitiesexperimentalstats/resultset.h>
#include <kactivitiesexperimentalstats/resultmodel.h>
#include <kactivitiesexperimentalstats/resultwatcher.h>
#include <kactivitiesexperimentalstats/terms.h>

#include <QDebug>
#include <QAction>
#include <QUrl>

namespace KAStats = KActivities::Experimental::Stats;

using namespace KAStats;
using namespace KAStats::Terms;

class TopContactsModel::Private {
public:
    Private(TopContactsModel *parent)
        : q(parent)
    {
    }

    struct ActionType {
        ActionType(int type)
            : _type(type)
        {
        }

        bool operator() (QAction *action) const
        {
            const auto &type = action->property("actionType");
            return !type.isNull() && type.toInt() == _type;
        }

        template <typename Collection>
        bool triggerFirstIn(const Collection &actions) const
        {
            const auto chosenAction = std::find_if(
                    actions.cbegin(), actions.cend(),
                    *this);

            if (chosenAction != actions.cend()) {
                (*chosenAction)->trigger();
                return true;
            }

            qDebug() << "Failed to find the action";
            return false;
        }

        int _type;
    };

    QHash<QString, KPeople::PersonData*> people;

    bool isOnline(const QString &id, KPeople::PersonData *person)
    {
        Q_UNUSED(id)

        const auto presence = person->presenceIconName();

        return presence == "user-online"
            || presence == "user-away"
            || presence == "user-busy";
    }

    void startChat(const QString &id, KPeople::PersonData *person)
    {
        qDebug() << "CHAT!";
        const auto actionList = KPeople::actionsForPerson(id, q);

        for (const auto& action: actionList) {
            qDebug() << "action: " << action->text();
        }
        ActionType(KPeople::ActionType::TextChatAction)
            .triggerFirstIn(actionList);
    }

    void composeEmail(const QString &id, KPeople::PersonData *person)
    {
        qDebug() << "Email!";
        const auto actionList = KPeople::actionsForPerson(id, q);
        ActionType(KPeople::ActionType::SendEmailAction)
            .triggerFirstIn(actionList);
    }

    void forgetContact(const QString &id, KPeople::PersonData *person)
    {
    }

    void forgetAll()
    {
    }

    void unlinkFromActivity(const QString &id, KPeople::PersonData *person)
    {
        watcher->unlinkFromActivity(QUrl(id));
    }

    void linkToActivity(const QString &id, KPeople::PersonData *person)
    {
        watcher->linkToActivity(QUrl(id));
    }

    TopContactsModel *q;
    KAStats::ResultWatcher *watcher;
};

TopContactsModel::TopContactsModel(QObject *parent)
    : QIdentityProxyModel(parent)
    , d(new Private(this))
{
#define RES(What) What##Resources
    auto query = RES(All) // AllResources // LinkedResources // UsedResources
                    | HighScoredFirst
                    | Agent("KTp")
                    | Url::startsWith("ktp")
                    ;

    setSourceModel(new ResultModel(query));
    d->watcher = new ResultWatcher(query, this);
}

TopContactsModel::~TopContactsModel()
{
    qDeleteAll(d->people);
}

QHash<int, QByteArray> TopContactsModel::roleNames() const
{
    return {
            { Qt::DisplayRole, "display" },
            { Qt::DecorationRole, "decoration" },
            { UrlRole, "url" },
            { AvatarPixmapRole, "avatarPixmap" },
            { PresenceRole, "presence" },
            { PresenceIconRole, "presenceIcon" },
            { IsLinkedRole, "isLinked" }
        };
}

QVariant TopContactsModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) return QVariant();

    auto contactId =
        sourceModel()->data(index, ResultModel::ResourceRole).toString();

    if (!d->people.contains(contactId)) {
        d->people[contactId] = new KPeople::PersonData(contactId);
    }

    const auto &contact = *d->people[contactId];

    return role == Qt::DisplayRole    ? contact.name()
         : role == Qt::DecorationRole ? contact.photo()
         : role == UrlRole            ? contactId
         : role == AvatarPixmapRole   ? contact.photo()
         : role == PresenceRole       ? contact.presence()
         : role == PresenceIconRole   ? contact.presenceIconName()
         : role == IsLinkedRole       ? (
                 QIdentityProxyModel::data(index, ResultModel::LinkStatusRole)
                    == ResultSet::Result::Linked)
         : QVariant();
}

void TopContactsModel::trigger(const QString &id, const QString &action)
{
    if (d->people.contains(id)) {
        auto contact = d->people[id];

        if (action.isEmpty()) {
            if (d->isOnline(id, contact)) {
                d->startChat(id, contact);
            } else {
                d->composeEmail(id, contact);
            }

        } else if (action == "chat") {
            d->startChat(id, contact);

        } else if (action == "email") {
            d->composeEmail(id, contact);

        } else if (action == "forget") {
            d->forgetContact(id, contact);

        } else if (action == "forgetAll") {
            d->forgetAll();

        } else if (action == "unlinkFromActivity") {
            d->unlinkFromActivity(id, contact);

        } else if (action == "linkToActivity") {
            d->linkToActivity(id, contact);

        }

    }
}


