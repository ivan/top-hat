/*
 *   Copyright (C) 2015 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#ifndef TOPCONTACTSMODEL_H
#define TOPCONTACTSMODEL_H

#include <QIdentityProxyModel>

class TopContactsModel: public QIdentityProxyModel {
    Q_OBJECT

public:
    TopContactsModel(QObject *parent = nullptr);
    ~TopContactsModel();

    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

    enum Roles {
        UrlRole          = Qt::UserRole + 1,
        AvatarPixmapRole = Qt::UserRole + 2,
        PresenceRole     = Qt::UserRole + 3,
        PresenceIconRole = Qt::UserRole + 4,
        IsLinkedRole     = Qt::UserRole + 5
    };

public Q_SLOTS:
    void trigger(const QString &id, const QString &action);

private:
    class Private;
    Private *const d;
};

#endif // TOPCONTACTSMODEL_H
